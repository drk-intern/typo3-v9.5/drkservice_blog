<?php
defined('TYPO3') or die();

// encapsulate all locally defined variables
(function ($extkey) {


    // Add flexform fiedl to plugin of tx_news
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools::class]['flexParsing'][]
        = \Drkservice\DrkBlog\EventListener\FlexFormEvent::class;

    $GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/News'][$extkey] = $extkey;

})('drkservice_blog');
