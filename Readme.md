# Blog

Diese Erweiterung stellt das DRK Blog dar

![image.png](Documentation%2Fimage.png)

## Installation

1. `composer req drkservice/blog`
2. TYPOScript im Template der betroffenen Seiten einfügen
3. PageTS im Template der betroffenen Seiten einfügen

## Nutzung

1. News anlegen
2. News plugin anlegen
   1. Storagefolder wählen
   2. Template auf Blog stellen
