<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$languageFile = 'LLL:EXT:drk_template/Resources/Private/Language/locallang_be.xlf';

$tempColumns = array(
    'frs_press_number' => array(
        'exclude' => 0,
        'label' => $languageFile . ':news.overrides.frs_press_number.label',
        'config' => array(
            'type' => 'input',
            'size' => '50',
            'eval' => 'trim',
        )
    ),
    'hide_press_lotto' => array(
        'exclude' => 0,
        'label' => $languageFile . ':news.overrides.hide_press_lotto.label',
        'config' => array(
            'type' => 'check'
        )
    ),
    'frs_country' => array(
        'exclude' => 0,
        'label' => $languageFile . ':news.overrides.frs_country.label',
        'config' => array(
            'type' => 'input',
            'size' => '50',
            'eval' => 'trim'
        )
    ),
    'frs_calltoaction' => array(
        'exclude' => 1,
        'label' => $languageFile . ':news.overrides.calltoaction.label',
        'config' => array(
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'tx_frsdrkcalltoaction_domain_model_main',
            'size' => '1',
            'maxitems' => '1',
            'minitems' => '0',
            'eval' => 'int',
            'default' => 0
        ),
    ),
    'show_anchor_menu' => [
        'exclude' => 0,
        'label' => $languageFile . ':news.overrides.show_anchor_menu.label',
        'config' => [
            'type' => 'check'
        ]
    ],
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tx_news_domain_model_news',
    $tempColumns
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tx_news_domain_model_news',
    'frs_country,--linebreak--,frs_press_number,hide_press_lotto,--linebreak--,frs_calltoaction',
    '',
    'after:datetime'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tx_news_domain_model_news',
    '--linebreak--,show_anchor_menu',
    '',
    'after:content_elements'
);