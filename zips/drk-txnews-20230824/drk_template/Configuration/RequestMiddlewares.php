<?php
return [
    'frontend' => [
        'frs/frs-drk-template/overwrite-meta-for-news' => [
            'target' => \Drk\Template\Middleware\OverwriteMetaForNewsMiddleware::class,
            'after' => [
                'frs/frs-drk-template/add-amp-attribute',
            ],
        ]
    ]
];
