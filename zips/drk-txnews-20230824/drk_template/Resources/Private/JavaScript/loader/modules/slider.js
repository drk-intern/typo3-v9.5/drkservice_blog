var Slider = {
  /**
   * Initialize
   */
  init: function () {
    var slidesToShow = 1,
      $sliderObjects = $('.js-slider').not('.slick-initialized'),
      $gallerySliderObjects = $('.js-gallery-slider').not('.slick-initialized'),
      $sliderArrowPrev,
      $sliderArrowPrevGallery,
      $sliderArrowNext,
      $sliderPlay,
      $sliderBreak,
      $sliderArrowNextGallery;
    $sliderArrowPrev =
      '<button type="button" class="c-news-slider__arrow c-news-slider__arrow--prev ' +
      'o-slider__arrow o-slider__arrow--prev slick-arrow" title="Rückschau" tabindex="0">' +
      '<span class="h-sr-only">Zurück</span>' +
      '<img src="/typo3conf/ext/drk_template/Resources/Public/Images/svg/arrow_left.svg" title="Rückschau" width="62" height="80">' +
      '<use xlink:href="/typo3conf/ext/drk_template/Resources/Public' +
      '/Images/svg/spritemap.svg#sprite-arrow-left-large">' +
      '</use></svg></button>';

    $sliderArrowNext =
      '<button type="button" class="c-news-slider__arrow c-news-slider__arrow--next ' +
      'o-slider__arrow o-slider__arrow--next slick-arrow" title="Vorschau" tabindex="0">' +
      '<span class="h-sr-only">Weiter</span>' +
      '<img src="/typo3conf/ext/drk_template/Resources/Public/Images/svg/arrow_right.svg" title="Vorschau" width="62" height="80">' +
      '<use xlink:href="/typo3conf/ext/drk_template/Resources/Public' +
      '/Images/svg/spritemap.svg#sprite-arrow-right-large">' +
      '</use></svg></button>';

    $sliderPlay =
      '<button type="button" class="c-news-slider__play h-hide" title="Abspielen des Sliders">' +
      '<span class="h-sr-only" aria-label="Symbolgrafik eines Play-Button">Abspielen</span>' +
      '<img src="/typo3conf/ext/drk_template/Resources/Public/Images/svg/play.svg" title="Play-Button" width="10" height="20">' +
      '</button>';
    $sliderBreak =
      '<button type="button" class="c-news-slider__break" title="Pausieren des Sliders">' +
      '<span class="h-sr-only" aria-label="Symbolgrafik eines Pause-Button">Pause</span>' +
      '<img src="/typo3conf/ext/drk_template/Resources/Public/Images/svg/pause.svg" title="Pause-Button" width="10" height="20">' +
      '</button>';

    $sliderArrowPrevGallery =
      '<button type="button" class="c-gallery-slider__arrow c-gallery-slider__arrow--prev ' +
      'o-slider__arrow o-slider__arrow--prev slick-arrow" title="Rückschau" tabindex="0">' +
      '<span class="h-sr-only">Zurück</span>' +
      '<svg role="img" class="o-slider__arrow-icon" aria-label="Symbolgrafik eines Pfeiles">' +
      '<use xlink:href="/typo3conf/ext/drk_template/Resources/Public' +
      '/Images/svg/spritemap.svg#sprite-arrow-left-large">' +
      '</use></svg></button>';

    $sliderArrowNextGallery =
      '<button type="button" class="c-gallery-slider__arrow c-gallery-slider__arrow--next ' +
      'o-slider__arrow o-slider__arrow--next slick-arrow" title="Vorschau" tabindex="0">' +
      '<span class="h-sr-only">Weiter</span>' +
      '<svg role="img" class="o-slider__arrow-icon" aria-label="Symbolgrafik eines Pfeiles">' +
      '<use xlink:href="/typo3conf/ext/drk_template/Resources/Public' +
      '/Images/svg/spritemap.svg#sprite-arrow-right-large">' +
      '</use></svg></button>';

    $sliderObjects.each(function () {
      slidesToShow = $(this).data('slides-to-show');
      var slideContainer = $(this).slick({
        autoplay: false,
        autoplaySpeed: 4000,
        accessibility: true,
        adaptiveHeight: true,
        dots: false,
        dotsClass: 'o-slider__nav',
        slide: '.o-slider__item, .js-slider-item, .o-section, .o-cms-content',
        infinite: true,
        slidesToScroll: slidesToShow,
        slidesToShow: slidesToShow,
        prevArrow: $sliderArrowPrev,
        nextArrow: $sliderArrowNext,
        pauseOnHover: true,
        useCSS: true,
        easing: 'swing',
        speed: 500,
        width: '100%',
        responsive: [
          {
            breakpoint: 720,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              mobileFirst: true,
              adaptiveHeight: false
            }
          }
        ]
      });
      var $sliderItems = slideContainer.find('.o-slider__item, .js-slider-item, .o-section, .o-cms-content');
      if ($sliderItems.length > 1 && $sliderItems.length > slidesToShow) {
        slideContainer.prepend($sliderBreak).prepend($sliderPlay);
        slideContainer.find('.c-news-slider__break').on('click', function () {
          if (!$(this).hasClass('h-hide')) {
            $(this).addClass('h-hide');
            slideContainer.find('.c-news-slider__play').removeClass('h-hide').focus();
          }
          if(slideContainer.hasClass('c-header-slider')) {
            ProgressBar.stopProgressbar();
          } else {
            slideContainer.slick('slickPause');
          }
        });
        slideContainer.find('.c-news-slider__play').on('click', function () {
          if (!$(this).hasClass('h-hide')) {
            $(this).addClass('h-hide');
            slideContainer.find('.c-news-slider__break').removeClass('h-hide').focus();
          }
          if (slideContainer.hasClass('c-header-slider')) {
            ProgressBar.proceedProgressbar();
          } else {
            slideContainer.slick('slickPlay');
          }
        });
        if (slideContainer.hasClass('c-header-slider')) {
          ProgressBar.init(slideContainer, $sliderItems);
          slideContainer.prepend(ProgressBar.$progressBarContainer);
          ProgressBar.startProgressbar();
        } else {
          slideContainer.slick('slickPlay');
        }
      }
      // @todo Prepare - image slider on screen out stop (optimize performance on client)
      // $(window).scroll(function(){
      //   var bTop = slideContainer.offset().top + slideContainer.height();
      //   if($(window).scrollTop()>=bTop){
      //     console.log('slick stop');
      //     if ($(this).hasClass('c-header-slider')) {
      //       ProgressBar.proceedProgressbar();
      //     } else {
      //       slideContainer.slick('slickPlay');
      //     }
      //   } else {
      //     console.log('slick start');
      //     if (slideContainer.hasClass('c-header-slider')) {
      //       ProgressBar.stopProgressbar();
      //     } else {
      //       slideContainer.slick('slickPause');
      //     }
      //   }
      // });
      if (slideContainer.find('.c-quick-donate__field').length > 0) {
        slideContainer.find('.c-quick-donate__field').each(function () {
          $(this).on('focus', function () {
            if (!slideContainer.find('.c-news-slider__break').hasClass('h-hide')) {
              slideContainer.find('.c-news-slider__break').addClass('h-hide');
              slideContainer.find('.c-news-slider__play').removeClass('h-hide');
            }
            if (slideContainer.hasClass('c-header-slider')) {
              ProgressBar.stopProgressbar();
            } else {
              slideContainer.slick('slickPause');
            }
          });
        });
      }
    });

    if ($gallerySliderObjects.length > 0) {
      $gallerySliderObjects.each(function () {
        var $this = $(this),
          $gallerImages = $this.find('img');
        if ($gallerImages.length > 1) {
          var slidesToShow = $(this).data('slides-to-show'),
            slideContainerGallery = $(this).slick({
              autoplay: false,
              autoplaySpeed: 4000,
              accessibility: true,
              adaptiveHeight: true,
              dots: false,
              dotsClass: 'o-slider__nav',
              slide: '.o-slider__item',
              infinite: true,
              slidesToScroll: slidesToShow,
              slidesToShow: slidesToShow,
              prevArrow: $sliderArrowPrevGallery,
              nextArrow: $sliderArrowNextGallery,
              pauseOnHover: true,
              useCSS: true,
              easing: 'swing',
              speed: 500,
              width: '100%',
              responsive: [
                {
                  breakpoint: 720,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    adaptiveHeight: false
                  }
                }
              ]
            });
          if (slideContainerGallery.find('.o-slider__item').length > 1 &&
            slideContainerGallery.find('.o-slider__item').length > slidesToShow) {
            slideContainerGallery.prepend($sliderBreak).prepend($sliderPlay);
            slideContainerGallery.find('.c-news-slider__break').on('click', function () {
              if (!$(this).hasClass('h-hide')) {
                $(this).addClass('h-hide');
                slideContainerGallery.find('.c-news-slider__play').removeClass('h-hide').focus();
              }
              slideContainerGallery.slick('slickPause');
            });
            slideContainerGallery.find('.c-news-slider__play').on('click', function () {
              if (!$(this).hasClass('h-hide')) {
                $(this).addClass('h-hide');
                slideContainerGallery.find('.c-news-slider__break').removeClass('h-hide').focus();
              }
              slideContainerGallery.slick('slickPlay');
            });
            slideContainerGallery.slick('slickPlay');
          }

          var $accordion = slideContainerGallery.parentsUntil('.o-accordion');
          if ($accordion.length > 0) {
            $accordion.find('.o-accordion__title').on('click', function () {
              slideContainerGallery.slick('slickNext');
            });
          }
          // slideContainerGallery.slick('slickPause');
        }
      });
    }
  },
};
// additional progressBar for slick slider
var ProgressBar = {
  percentTime: 0,
  tick: 0,
  time: .1,
  progressBarIndex: 0,
  sliderItems: null,
  $slideContainer: null,
  $progressBarContainer: null,
  init: function ($slideContainer, $sliderItems) {
    this.$slideContainer = $slideContainer;
    this.$sliderItems = $sliderItems;
    var $progressBarContainer = $('<div />').addClass('progressBarContainer');
    this.$sliderItems.each(function(index) {
      $progressBarContainer.append($('<div />').addClass('progress processing-' + index));
    });
    this.$progressBarContainer = $progressBarContainer;
  },
  startProgressbar: function() {
    this.resetProgressbar();
    this.percentTime = 0;
    this.time = 0;
    this.tick = setInterval(this.interval.bind(this), 10);
  },
  proceedProgressbar: function() {
    this.tick = setInterval(this.interval.bind(this), 10);
  },
  stopProgressbar: function() {
    clearInterval(this.tick);
  },
  resetProgressbar: function() {
    this.$progressBarContainer.find('.progress').css({
      width: 0 + '%'
    });
    clearInterval(this.tick);
  },
  interval: function() {
    if ((this.$slideContainer.find('.slick-track div[data-slick-index="' + this.progressBarIndex + '"]').attr("aria-hidden")) === "true") {
      this.progressBarIndex = ProgressBar.$slideContainer.find('.slick-track div[aria-hidden="false"]').data("slickIndex");
      this.startProgressbar();
    } else {
      this.percentTime += 1.5 / (this.time + 6);
      this.$progressBarContainer.find('.processing-' + this.progressBarIndex).css({
        width: this.percentTime + "%"
      });
      if (this.percentTime >= 100) {
        this.$slideContainer.slick('slickNext');
        this.progressBarIndex++;
        if (this.progressBarIndex > 2) {
          this.progressBarIndex = 0;
        }
        this.startProgressbar();
      }
    }
  }
};

