<?php
declare(strict_types = 1);

return [
    \GeorgRinger\News\Domain\Model\News::class => [
        'subclasses' => [
            \Drkservice\DrkBlog\Domain\Model\NewsBlog::RECORD_TYPE_ID => \Drkservice\DrkBlog\Domain\Model\NewsBlog::class,
        ]
    ],
    \Drkservice\DrkBlog\Domain\Model\PlusdrkbaseNewsauthor::class => [
        'tableName' => 'tx_plusdrkbase_domain_model_newsauthor',
    ],
    \Drkservice\DrkBlog\Domain\Model\NewsBlog::class => [
        'tableName' => 'tx_news_domain_model_news',
        'recordType' => \Drkservice\DrkBlog\Domain\Model\NewsBlog::RECORD_TYPE_ID,
    ],
];
