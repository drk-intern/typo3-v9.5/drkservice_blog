<?php
defined('TYPO3') or die();


$boot = static function (int $redordTypeId, string $label): void {
    $ll = 'LLL:EXT:drkservice_blog/Resources/Private/Language/locallang_be_news.xlf';

    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['type']['config']['items'][(string)$redordTypeId] =
        [
            'label' => $label,
            'value' => $redordTypeId
        ];

    $GLOBALS['TCA']['sys_category']['columns']['shortcut']['label'] = $ll . ':news.overrides.category_link.label';
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['categories']['config']['size'] = 25;

    $tempColumns = [
        'frs_press_number' => [
            'exclude' => 0,
            'label' => $ll . ':news.overrides.frs_press_number.label',
            'config' => [
                'type' => 'input',
                'size' => '50',
                'eval' => 'trim',
            ]
        ],
        'hide_press_lotto' => [
            'exclude' => 0,
            'label' => $ll . ':news.overrides.hide_press_lotto.label',
            'config' => [
                'type' => 'check'
            ]
        ],
        'frs_country' => [
            'exclude' => 0,
            'label' => $ll . ':news.overrides.frs_country.label',
            'config' => [
                'type' => 'input',
                'size' => '50',
                'eval' => 'trim'
            ]
        ],
        'frs_calltoaction' => [
            'exclude' => 1,
            'label' => $ll . ':news.overrides.calltoaction.label',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_frsdrkcalltoaction_domain_model_main',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0',
                'eval' => 'int',
                'default' => 0
            ],
        ],
        'show_anchor_menu' => [
            'exclude' => 0,
            'label' => $ll . ':news.overrides.show_anchor_menu.label',
            'config' => [
                'type' => 'check'
            ]
        ],
        'plusdrkbase_newsauthor' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:drkservice_blog/Resources/Private/Language/locallang_db.xlf:tx_plusdrkbase_domain_model_newsauthor.news_authorprofil',
            'description' => 'LLL:EXT:drkservice_blog/Resources/Private/Language/locallang_db.xlf:tx_plusdrkbase_domain_model_newsauthor.news_authorprofil.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_plusdrkbase_domain_model_newsauthor',
                'foreign_table_where' => ' AND (tx_plusdrkbase_domain_model_newsauthor.sys_language_uid IN (-1,0) OR tx_plusdrkbase_domain_model_newsauthor.l10n_parent = 0) ORDER BY tx_plusdrkbase_domain_model_newsauthor.lastname',
                'MM' => 'tx_plusdrkbase_domain_model_newsauthor_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 1,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tx_news_domain_model_news',
        $tempColumns
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tx_news_domain_model_news',
        'frs_country,--linebreak--,frs_press_number,hide_press_lotto,--linebreak--,frs_calltoaction,',
        '',
        'after:datetime'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tx_news_domain_model_news',
        '--linebreak--,show_anchor_menu,',
        '',
        'after:content_elements'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tx_news_domain_model_news',
        '--linebreak--,plusdrkbase_newsauthor,',
        '',
        ''
    );
};

$boot(
    \Drkservice\DrkBlog\Domain\Model\NewsBlog::RECORD_TYPE_ID,
    'Blogpost'
);
unset($boot);
