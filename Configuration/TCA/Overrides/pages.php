<?php

defined('TYPO3') or die();
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

(function() {
    ExtensionManagementUtility::registerPageTSConfigFile(
        'drkservice_blog',
        'Configuration/PageTS/PageTSconfig.typoscript',
        'DRK Blog'
    );

    ExtensionManagementUtility::registerPageTSConfigFile(
        'drkservice_blog',
        'Configuration/PageTS/BlogPostReducedFields.typoscript',
        'DRK Blog - reduced fields'
    );
})();

