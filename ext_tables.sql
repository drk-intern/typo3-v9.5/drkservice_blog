#
# Table structure for table 'tx_news_domain_model_news'
#
CREATE TABLE tx_news_domain_model_news (
	frs_press_number text,
	hide_press_lotto tinyint(4) DEFAULT '0' NOT NULL,
	frs_country text,
	frs_calltoaction int(11) unsigned DEFAULT '0' NOT NULL,
	show_anchor_menu tinyint(4) DEFAULT '0' NOT NULL,
	plusdrkbase_newsauthor int(11) unsigned DEFAULT '0',
);

#
# Table structure for table 'tx_plusdrkbase_domain_model_newsauthor'
#
CREATE TABLE tx_plusdrkbase_domain_model_newsauthor (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	editlock tinyint(4) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumtext,
	l10n_source int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

    firstname varchar(255) DEFAULT '' NOT NULL,
    lastname varchar(255) DEFAULT '' NOT NULL,
    slug varchar(2048),
    email varchar(255) DEFAULT '' NOT NULL,
    text text NOT NULL,
    image int(11) DEFAULT '0' NOT NULL,
    news int(11) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY sys_language_uid_l10n_parent (sys_language_uid,l10n_parent),
);

#
# Table structure for table 'tx_plusdrkbase_domain_model_newsauthor_mm'
#
CREATE TABLE tx_plusdrkbase_domain_model_newsauthor_mm (
    uid_local int(11) unsigned DEFAULT '0' NOT NULL,
    uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
    sorting int(11) unsigned DEFAULT '0' NOT NULL,
    sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

    KEY uid_local (uid_local),
    KEY uid_foreign (uid_foreign)
);
