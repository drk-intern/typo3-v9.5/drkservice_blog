<?php
namespace Drkservice\DrkBlog\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Http\Stream;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class OverwriteMetaForNewsMiddleware implements MiddlewareInterface
{
    /**
     * Add amp to style tag
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var ResponseInterface $response */
        $response = $handler->handle($request);

        $a=1;
        if (
            !($response instanceof \TYPO3\CMS\Core\Http\NullResponse)
            && $GLOBALS['TSFE'] instanceof \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController
            && isset($request->getQueryParams()['tx_news_pi1']['action'])
            && $request->getQueryParams()['tx_news_pi1']['action'] === 'detail') {
            /** @var Stream $responseBody */
            $responseBody = new Stream('php://temp', 'rw');

            /** @var string $html */
            $html = $response->getBody()->__toString();
            $html = $this->replaceNoindex($html);

            // Replace old body with $html
            $responseBody->write($html);

            $response = $response->withBody($responseBody);
        }

        return $response;
    }

    /**
     * @param string $html
     * @return string
     */
    private function replaceNoindex($html = ''): string
    {
        return str_replace('<meta name="robots" content="noindex,', '<meta name="robots" content="index,', $html);
    }
}
