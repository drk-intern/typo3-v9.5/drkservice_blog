<?php
namespace Drkservice\DrkBlog\EventListener;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2020 Rico Nitschke <rico.nitschke@e-pixler.com>, e-pixler NEW MEDIA GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\Statement;
use Drk\Credits\Service\Import\ImportService;
use Drk\Credits\Domain\Repository\NewsRepository;
use Drk\Calltoaction\Domain\Repository\MainRepository;
use Drkservice\DrkBlog\Domain\Model\NewsBlog;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\Exception\RequiredArgumentMissingException;

/**
 * Example for custom event listener
 *
 * Add own credits and references to the credits table & referrals
 *
 * Return an array with credits based on \Drk\Credits\Domain\Model\Credit
 *
 * Class NewsEventListener
 * @package Drkservice\DrkBlog\EventListener\NewsEventListener
 */

class NewsEventListener extends ImportService
{

	/** @const EXTENSION */
	const EXTENSION = 'news';

	/** @const DETAIL_PID */
	const DETAIL_PID = 654;

	/** @const STORAGE_PID */
	const STORAGE_PID = 648;

	/** @const TABLE */
	const TABLE = 'tx_news_domain_model_news';

	/** @const FIELD_WITH_RECORD_PID */
	const FIELD_WITH_RECORD_PID = 'frs_calltoaction';

	/** @const TABLE_REF Reference table, where the main media reference is stored in sys_file_reference */
	const TABLE_REF = 'tx_frsdrkcalltoaction_domain_model_main';

	/** @var ConnectionPool|null $connectionPool */
	protected $connectionPool = null;

	/** @var \Drk\Calltoaction\Domain\Repository\MainRepository $mainRepository */
	protected $mainRepository = null;

	/**
	 * @return bool
	 */
	public function excute()
	{
		$this->initialize();
		return $this->checkFieldInTable();
	}

	protected function initialize()
	{
		$this->connectionPool = $this->objectManager->get(ConnectionPool::class);
		$this->mainRepository = $this->objectManager->get(MainRepository::class);
	}

	/**
	 * @return bool
	 */
	protected function checkFieldInTable(): bool
	{
		/** @var News[] $newsRecords */
		$newsRecords = $this->getRecords();
		if (isset($newsRecords) && count($newsRecords) > 0) {
			/**
			 * @param int $keyUid
			 * @param array $recordValues
			 */
			foreach($newsRecords as $keyUid => $recordValues) {
				$refRecord = $this->mainRepository->findByUid((int) $recordValues[self::FIELD_WITH_RECORD_PID]);
				if (isset($refRecord)) {
					/** @var array $uidForeign */
					$uidForeign = [0 => $refRecord->getUid()];
					/** @var array $fileReferences */
					$fileReferences = $this->sysFileReferenceRepository->findMediaByUidForeign(
						$uidForeign,
						self::TABLE_REF
					);
					/** @var string $title */
					$title = $this->getCreditCollectionUtility()->getPageArrayKey(self::DETAIL_PID, $recordValues['sys_language_uid']);
					/** @var \DateTime $tstamp */
					$tstamp = new \DateTime;
					/** @var int $detailPid */
					$detailPid = self::DETAIL_PID;
					$this->getCreditCollectionUtility()->setRecordArrayKey(self::DETAIL_PID, $title);
					$this->getCreditCollectionUtility()->checkReferenceCredits(
						$fileReferences,
						$detailPid,
						self::TABLE,
						$recordValues['sys_language_uid'],
						$tstamp->setTimestamp((int) $recordValues['tstamp'])
					);
					$this->getCreditCollectionUtility()->unsetRecordArrayKey(self::DETAIL_PID);
				}
				unset($newsRecords[$keyUid]);
			}
			return true;
		}
		return false;
	}

	/**
	 * @return Statement|int
	 */
	public function getRecords()
	{
		/** @var Connection $connection */
		$connection = $this->connectionPool->getConnectionForTable(self::TABLE);
		/** @var QueryBuilder $queryBuilder */
		$queryBuilder = $connection->createQueryBuilder();
		/** @var Statement|int $parentStatement */
		$parentStatement = $queryBuilder
			->select('*')
			->from(self::TABLE)
			->where(
				$queryBuilder->expr()->neq(self::FIELD_WITH_RECORD_PID, 0),
				$queryBuilder->expr()->in('pid', self::STORAGE_PID)
			)
			->execute();
		return $parentStatement->fetchAll();
	}
}
