<?php

namespace Drkservice\DrkBlog\EventListener;

use TYPO3\CMS\Core\Configuration\Event\AfterFlexFormDataStructureParsedEvent;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FlexFormEvent {

    public function parseDataStructureByIdentifierPostProcess(array $dataStructure, array $identifier): array
    {
        if ($identifier['type'] === 'tca'
            && $identifier['tableName'] === 'tt_content'
            && $identifier['fieldName'] === 'pi_flexform'
            && $identifier['dataStructureKey'] === 'news_pi1,list'
        ) {
            $file = GeneralUtility::getFileAbsFileName('EXT:drkservice_blog/Configuration/FlexForms/Txnews_list.xml');
            $content = file_get_contents($file);

            if ($content) {
                $dataStructure['sheets']['template']['ROOT']['el']['settings.replaceByLabel'] =
                    \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($content);
            }
        }
        return $dataStructure;
    }
}
