<?php

namespace Drkservice\DrkBlog\Domain\Model;


use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2023 Doreen Gröger
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * News
 */
class NewsBlog extends \GeorgRinger\News\Domain\Model\News
{
    /**
     * Magic Number to identify the RECORD_TYPE_ID in the tx_news table
     */
    public const RECORD_TYPE_ID = 1700557640;

    /**
     * plusdrkbaseNewsauthor
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Drkservice\DrkBlog\Domain\Model\PlusdrkbaseNewsauthor>
     */
    protected $plusdrkbaseNewsauthor = null;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * Initializes all \TYPO3\CMS\Extbase\Persistence\ObjectStorage properties.
     *
     * @return void
     */
    public function initStorageObjects():void
    {
        $this->plusdrkbaseNewsauthor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage;
    }

    /**
     * Adds a PlusdrkbaseNewsauthor
     *
     * @param \Drkservice\DrkBlog\Domain\Model\PlusdrkbaseNewsauthor $plusdrkbaseNewsauthor
     * @return void
     */
    public function addPlusdrkbaseNewsauthor(\Drkservice\DrkBlog\Domain\Model\PlusdrkbaseNewsauthor $plusdrkbaseNewsauthor)
    {
        $this->plusdrkbaseNewsauthor->attach($plusdrkbaseNewsauthor);
    }

    /**
     * Removes a PlusdrkbaseNewsauthor
     *
     * @param \Drkservice\DrkBlog\Domain\Model\PlusdrkbaseNewsauthor $plusdrkbaseNewsauthorToRemove The PlusdrkbaseNewsauthor to be removed
     * @return void
     */
    public function removePlusdrkbaseNewsauthor(\Drkservice\DrkBlog\Domain\Model\PlusdrkbaseNewsauthor $plusdrkbaseNewsauthorToRemove)
    {
        $this->plusdrkbaseNewsauthor->detach($plusdrkbaseNewsauthorToRemove);
    }

    /**
     * Returns the plusdrkbaseNewsauthor
     *
     * @return ObjectStorage|null
     */
    public function getPlusdrkbaseNewsauthor()
    {
        return $this->plusdrkbaseNewsauthor;
    }

    /**
     * Sets the plusdrkbaseNewsauthor
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Drkservice\DrkBlog\Domain\Model\PlusdrkbaseNewsauthor> $plusdrkbaseNewsauthor
     * @return void
     */
    public function setPlusdrkbaseNewsauthor(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $plusdrkbaseNewsauthor)
    {
        $this->plusdrkbaseNewsauthor = $plusdrkbaseNewsauthor;
    }

}
