<?php

namespace Drkservice\DrkBlog\Domain\Model;

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Authors for tx_news
 */
class PlusdrkbaseNewsauthor extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * firstname
     *
     * @var string
     */
    protected $firstname = '';

    /**
     * lastname
     *
     * @var string
     */
    protected $lastname = '';

    /**
     * slug
     *
     * @var string
     */
    protected $slug = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * text
     *
     * @var string
     */
    protected $text = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image;

    /**
     * news
     *
     * @var ObjectStorage<\Drkservice\DrkBlog\Domain\Model\NewsBlog>
     */
    protected $news;

    /**
     * posts is used to build final news
     *
     * @var array
     */
    protected $posts;

    /**
     * Initializes all \TYPO3\CMS\Extbase\Persistence\ObjectStorage properties.
     *
     * @return void
     */
    public function initStorageObjects():void
    {
        $this->news = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage;
    }

    /**
     * Returns the firstname
     *
     * @return string $firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Sets the firstname
     *
     * @param string $firstname
     * @return void
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Returns the lastname
     *
     * @return string $lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Sets the lastname
     *
     * @param string $lastname
     * @return void
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Returns the slug
     *
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Sets the slug
     *
     * @param string $slug
     * @return void
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the image
     *
     * @return ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
     * @return void
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Adds a NewsBlog
     *
     * @param \Drkservice\DrkBlog\Domain\Model\NewsBlog $news
     * @return void
     */
    public function addNews(\Drkservice\DrkBlog\Domain\Model\NewsBlog $news)
    {
        $this->newsblog->attach($news);
    }

    /**
     * Removes a News
     *
     * @param \Drkservice\DrkBlog\Domain\Model\NewsBlog $newsToRemove The News to be removed
     * @return void
     */
    public function removeNews(\Drkservice\DrkBlog\Domain\Model\NewsBlog $newsToRemove)
    {
        $this->newsblog->detach($newsToRemove);
    }



    /**
     * Returns the news
     *
     * @return ObjectStorage<\Drkservice\DrkBlog\Domain\Model\NewsBlog> $neNewsBlog
     */
    public function getNews()
    {
        return $this->newsblog;
    }

    /**
     * Sets the news
     *
     * @param ObjectStorage<\Drkservice\DrkBlog\Domain\Model\NewsBlog> $news
     * @return void
     */
    public function setNews(ObjectStorage $news)
    {
        $this->newsblog = $news;
    }

    /**
     * Returns posts from the same author
     * Used on blog detail page at the end
     *
     * @return array
     */
    public function getPosts():array
    {
        // other news of the author
        $allAuthorPosts = $this->newsblog->toArray();

        // show the maximum posts of the author + plus the current one
        // e.g. max 4 post of the author should be displayed and add one
        // the current post is unknown at this point
        $maxPost = 5;

        if (count($allAuthorPosts) === 1) {
            // if there is just one post, it is the current detail page
            $posts = [];
        } else {
            $i = 0;
            foreach ($allAuthorPosts as $key => $singleAuthorPost) {
                if ($i < 5) {
                    $posts[$key]['uid'] = $singleAuthorPost->getUid();
                    $posts[$key]['title'] = $singleAuthorPost->getTitle();

                    // get image of news listview (not the image of the detailpage)
                    $newsImages = $singleAuthorPost->getFalMedia();
                    foreach ($newsImages as $newsImage) {
                        if ($newsImage->getShowinpreview() > 0) {
                            $posts[$key]['image'] = $newsImage;
                            break;
                        }
                    }
                    $posts[$key]['datetime'] = $singleAuthorPost->getDatetime();
                    $posts[$key]['slug'] = $singleAuthorPost->getPathSegment();
                }
                $i++;
            }

            // sort posts of the author
            usort($posts, fn($a, $b) => $a['datetime'] <=> $b['datetime']);
            krsort($posts);
            $posts = array_values($posts);
        }

        return $posts;
    }
}
