<?php
/**
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 */

namespace Drkservice\DrkBlog\Domain\Model;

/**
 * Class News
 *
 * @notice do not edit code format
 * @package Drkservice\DrkBlog\Domain\Model
 */
class NewsFileReference extends \GeorgRinger\News\Domain\Model\FileReference
{

    /**
     * @var string
     */
    protected $frsShowCopyright;

    /**
     * @param string $frsShowCopyright
     */
    public function setFrsShowCopyright($frsShowCopyright)
    {
        $this->frsShowCopyright = $frsShowCopyright;
    }

    /**
     * @return string
     */
    public function getFrsShowCopyright()
    {
        return $this->frsShowCopyright;
    }
}
