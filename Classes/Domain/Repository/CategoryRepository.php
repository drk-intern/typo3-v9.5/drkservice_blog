<?php

namespace Drkservice\DrkBlog\Domain\Repository;

use GeorgRinger\News\Domain\Repository\CategoryRepository as GeorgeCategoryRepository;
use GeorgRinger\News\Service\CategoryService;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

class CategoryRepository extends GeorgeCategoryRepository
{

    /**
     * Find category tree
     *
     * @param array $rootIdList list of id s
     *
     * @return QueryInterface
     */
    public function findTree(array $rootIdList, $startingPoint = null)
    {
        $subCategories = CategoryService::getChildrenCategories(implode(',', $rootIdList));
        $ordering = ['title' => QueryInterface::ORDER_ASCENDING];

        $categories = $this->findByIdList(explode(',', $subCategories), $ordering);
        $flatCategories = [];
        foreach ($categories as $category) {
            $flatCategories[$category->getUid()] = [
                'item' => $category,
                'parent' => ($category->getParentcategory()) ? $category->getParentcategory()->getUid() : null
            ];
        }

        $tree = [];

        // If leaves are selected without its parents selected, those are shown as parent
        foreach ($flatCategories as $id => &$flatCategory) {
            if (!isset($flatCategories[$flatCategory['parent']])) {
                $flatCategory['parent'] = null;
            }
        }

        foreach ($flatCategories as $id => &$node) {
            if ($node['parent'] === null) {
                $tree[$id] = &$node;
            } else {
                $flatCategories[$node['parent']]['children'][$id] = &$node;
            }
        }
        return $tree;
    }
}
